//WAP to find the sum of two fractions.

#include<stdio.h>
struct fract
{
int num;
int deno;
};
typedef struct fract fraction;
fraction input()
{
fraction a;
printf("Enter the numerator\n");
scanf("%d",&a.num);
printf("Enter the denominator\n");
scanf("%d",&a.deno);
return a;
}
int GCD(fraction k)
{
int gcd,i;
for(i=1;i<=k.num && i<=k.deno;i++)
if(k.num%i==0 && k.deno%i==0)
{
gcd=i;
}
}
fraction sum(fraction a,fraction b)
{
fraction res;
int gcd;
if(a.deno==b.deno)
{
res.deno=a.deno;
res.num=a.num+b.num;
}
else
{
	res.deno=a.deno*b.deno;
	res.num=(a.num*b.deno)+(b.num*a.deno);
}
gcd=GCD(res);
res.num=res.num/gcd;
res.deno=res.deno/gcd;
	return res;
}
void output(fraction a,fraction b,fraction c)
{
	printf("The sum of %d/%d and %d/%d is %d/%d",a.num,a.deno,b.num,b.deno,c.num,c.deno);
}
int main()
{
	fraction a,b,c;
	a=input();
	b=input();
	c=sum(a,b);
	output(a,b,c);
	return 0;
}

