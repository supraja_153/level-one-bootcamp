//WAP to find the sum of n fractions.

#include<stdio.h>
typedef struct
{
int num;
int deno;
}fract;
fract input()
{
fract a;
printf("Enter the numerator\n");
scanf("%d",&a.num);
printf("Enter the denominator\n");
	scanf("%d",&a.deno);
	return a;
}
int hcf(fract a)
{
	int gcd,i;
	for(i=1;i<=a.num && i<=a.deno;i++)
	{
		if(a.num%i==0 && a.deno%i==0)
		gcd=i;
	}
	return gcd;
}
fract sum(fract p,fract q)
{
	fract s;
	int gcd;
	if(p.deno==q.deno)
	{
		s.deno=p.deno;
		s.num=p.num+q.num;
	}
	else
	{
		s.deno=p.deno*q.deno;
		s.num=(p.num*q.deno)+(q.num*p.deno);
	}
	gcd=hcf(s);
	s.num=s.num/gcd;
	s.deno=s.deno/gcd;
	return s;
}
void output(fract s)
{
printf("The final sum is %d/%d which is equal to %.2f \n", s.num ,s.deno,
(s.num /(1.0*s.deno)));	
}
int main()
{
	int n;
	printf("Enter the number of fractions\n");
	scanf("%d",&n);
	fract s,a[n];
	s.num=0;
	s.deno=1;
	for(int j=0;j<n;j++)
	{
		printf("For fraction %d\n",(j+1));
		a[j]=input();
	}
	for(int j=0;j<n;j++)
	{
		s=sum(s,a[j]);
	}
	output(s);
	return 0;
}
