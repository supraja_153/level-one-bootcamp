//WAP to find the volume of a tromboloid using 4 functions.
#include<stdio.h>
float input()
{
float n;
printf("Enter the value : ");
scanf("%f",&n);
return n;
}
float find_volume(float h,float d,float b)
{
float volume;
volume = (((h*d*b)+(d/b))/3);
return volume;
}
void output(float h,float d,float b,float n)
{
printf("volume of the trombolid with height=%f,depth=%f,breadth=%f is :%f\n",h,d,b,n);
}
int main()
{
float h,d,b,v;
h=input();
d=input();
b=input();
v=find_volume(h,d,b);
output(h,d,b,v);
return 0;
}