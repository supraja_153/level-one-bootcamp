//WAP to find the distance between two point using 4 functions.
#include <stdio.h>
#include <math.h>
int input1()
{
    float x;
    scanf("%f",&x);
    return x;
}
int input2()
{
    float y;
    scanf("%f",&y);
    return y;
}
float find_distance(float x1,float x2,float y1,float y2,float distance)
{
    distance=sqrt((x2-x1)*(x2-x1))+((y2-y1)*(y2-y1));
    return distance;
}
void output(float x1,float y1,float x2,float y2,float dist)
{
    printf("Distance between the two points (%f,%f) and (%f,%f) is : %f\n",x1,y1,x2,y2,dist);
}
int main()
{
    float x1,y1,x2,y2,distance;
    printf("Enter the value of x1 :");
    x1=input1();
    printf("Enter the value of y1 :");
    y1=input2();
    printf("Enter the value of x2 :");
    x2=input1();
    printf("Enter the value of y2 :");
    y2=input1();
    float dist = find_distance(x1,y1,x2,y2,distance);
    output(x1,y1,x2,y2,dist);
    return 0;
}